var 
    gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename')
;

gulp.task('default', function() {
    return gulp.src('./lib/client.js')
        .pipe(uglify())
        .pipe(rename('client.min.js'))
        .pipe(gulp.dest('./lib'))
    ;
});