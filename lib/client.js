(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.PACAd = factory();
  }
}(this, function () {

    function PACAd(name, origin, context) {
        this.$$init(name, origin, context);
    }
    PACAd.instanceCount = 0;
    PACAd.version = "0.1.0";

    PACAd.prototype.on = function(type, callback) {
        type = type.toLowerCase();
        if( !this.notifications[type] ){
            this.notifications[type] = [];
        }
        this.notifications[type].push(callback);
    };

    PACAd.prototype.send = function(type, data) {
        this.$$sendPayload(type, data);
    };

    PACAd.prototype.$$init = function(name, origin, context) {
        var _self = this;
        this.name = name || 'instance'+ (++PACAd.instanceCount);
        this.context = context || window.parent;
        this.origin = origin || "*";
        this.notifications = {};
        window.addEventListener('message', this.$$handleMessage.bind(_self));
    };

    PACAd.prototype.$$handleMessage = function(e) {
        var 
            payload = JSON.parse(e.data),
            name = payload.name,
            type = payload.type.split(".")[1].toLowerCase(),
            data = payload.data
        ;

        if( !!this.notifications[type] && this.notifications[type].length) {
            var list = this.notifications[type].slice();
            var callback = null;
            while(!!(callback = list.shift())) {
                callback(payload);
            }
        }
    };

    PACAd.prototype.$$sendPayload = function(type, data) {
        var payload = JSON.stringify({
            name: this.name,
            type: "PACAd." + type.toUpperCase(),
            data: data
        });
        this.context.postMessage(payload, this.origin);
    };

    return PACAd;
}));