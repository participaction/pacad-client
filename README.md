**participaction**

# PACAd-Client

## Installation

Install package with NPM and add it to your dependencies:

```bash
npm install --save pacad 
```

## CDN

You may use the CDN to include this in your creative (recommended).  
Checkout some [samples here](https://bitbucket.org/participaction/pacad-samples)

```html
<script src="https://d11lfr9wsxo2ux.cloudfront.net/lib/pacad/client.min.js"></script>
```

## Usage

### PACad(_unitName_)

Construct a new client

Arguments | Description
---: | :---
_unitName_ | the name of the unit (no-spaces or punctuation)

**example**

```js
var client = new PACAd('myUnitName');
```



### PACAd.on(_eventType_, _callback_)

Listen for events

Arguments | Description
---: | :---
_eventType_ | The event to listen to (lowercase)
_callback_ | The callback function to invoke when the _eventType_ is heard

**example**

```js
var client = new PACAd('myUnitName');

client.on('loaded', function(event) {
    console.log('PACAd was loaded')
});
```



### PACAd.send(_eventType_, _data_)

Trigger and event

Arguments | Description
---: | :---
_eventType_ | The event to trigger/send (lowercase)
_data_ | The data-object to send with the event.

**example**

```js
var client = new PACAd('myUnitName');

client.send('clicktag', {url: "http://www.domain.com/my/path"});
```



### Event-Object

All events return an event-object with the following properties:

Property | Description
---: | :---
_name_ | The name of the PACAd. If this is from the main window, the name will be `PACAd`
_type_ | The event-type. (lowercased) 
_data_ | Non-opinionated data-object.


### Event-Types

#### PACAd.LOADED ("loaded")

Fired when the iFrame has loaded.

Properties | Values
:--- | :---
initialized | TRUE \| FALSE


#### PACAd.STATE_CHANGE ("state_changed")

Fired when the state fo the Unit-Wrapper has changed (closed or opened)

Properties | Values
:--- | :---
_state_| closed \| opened



